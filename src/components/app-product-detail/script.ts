export default {
    name: 'app-product-detail',
    props: {
        product: {
            type: Object
        }
    },
    setup(props, context){

        const addToCart = (ev) => {
            context.emit('add', ev)
        }
        return {
            addToCart
        }
    }
}