import { storeToRefs } from 'pinia'
import { useCartStore } from '@/stores/cart'
import { useUserStore } from '@/stores/user'
import { useRouter } from 'vue-router'
export default {
    name: 'app-header',
    setup(){
        const router = useRouter()
        const cartStore = useCartStore()
        const userStore = useUserStore()
        const { isUserLogged } = storeToRefs(userStore)
        const { cartCount } = storeToRefs(cartStore)

        const routes = [
            {
                path: '/home',
                label: 'home'
            }
        ]

        const routesRight = [
            {
                path: '/cart',
                label: 'cart'
            },
            {
                action: () => doLogout(),
                label: 'log out'
            }
        ]

        const doLogout = () => {
            userStore.logout()
            router.push({name: 'login'})

        } 

        return {
            isUserLogged,
            cartCount,
            routes,
            routesRight
        }
    }
}