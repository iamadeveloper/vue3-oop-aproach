import {reactive, onMounted} from 'vue'
import { useProducts } from '@/composables/services/useProducts'
import Category from '@/models/Category'

export default {
    name: 'app-categories',
    props: {
        modelValue: {
            type: String
        }
    },
    setup(props, context){
        const { getCategories} = useProducts()
        const categories = reactive([])
        const categorySelected = 'all'

        const fetchCategories = async () => {
            const categoryList = await getCategories()
            return categories.values = categoryList.map(cat => new Category(cat))
        }

        const configComponent = () => {
            fetchCategories()
        }

        const emitValue = (ev) => {
            context.emit('update:modelValue', ev.target.value)
        }

        onMounted(() => {
            configComponent()
        })
        
        return {
            categories,
            categorySelected,
            emitValue
        }
    }
}