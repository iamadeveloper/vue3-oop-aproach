import { createRouter, createWebHistory } from 'vue-router'
import * as jsonRoutes from './routes.json';


const routes = jsonRoutes.default

console.log(routes)




const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: routes.map(route => {
    const hasChildren = route.children
    return {
      name: route.name,
      path: route.path,
      component: () => import(`../${route.component}`)
    }
  })
})

export default router
