export enum PaymentType {
    Credit = 'credit',
    Paypal = 'paypal',
}