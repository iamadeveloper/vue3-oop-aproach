import type { ICategory } from "@/contracts/ICategory"
import type { IProduct } from "@/contracts/IProduct"

export default class Category implements ICategory{

    private _category

    constructor(cat: ''){
        this._category = cat
    }

    public get category(){
        return this._category
    }

    public getProducts(products: IProduct[]){
        return products.filter(product => product.category === this.category)
    }
}