import CartItem from "./CartItem";
import type {IUser} from "./contracts/IUser";

export default class Invoice{
    private _id: number
    private _date: Date
    private _user: IUser
    private _items: CartItem[]
    private _orderId: number
    private _total: number

    constructor(date: Date, user: IUser, items: CartItem[], orderId: number, total: number){
        this._id = new Date().getMilliseconds()
        this._date = date
        this._user = user
        this._items = items,
        this._orderId = orderId
        this._total = total
    }

    public getInvoiceDetails(){
        return {
            id: this._id,
            date: this._date,
            user: this._user,
            items: this._items,
            orderId: this._orderId,
            total: this._total
        }
    }
}