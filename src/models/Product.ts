import type { IProduct } from "@/contracts/IProduct";

export default class Product implements IProduct{
    private _id: number = 0;
    private _title: string = '';
    private _price: number = 0;
    private _description: string = '';
    private _category: string = '';
    private _image: string = '';
    private _rating: object = {};
    private _vat: number;

    constructor(product: IProduct, vat: number = 21){
        this._id = product.id;
        this._title = product.title;
        this._price = product.price;
        this._description = product.description;
        this._category = product.category;
        this._image = product.image;
        this._rating = product.rating;
        this._vat = vat
    }

    public get id(){
        return this._id
    }

    public get title(){
        return this._title
    }

    public get price(){
        return this._price
    }

    public get description(){
        return this._description
    }

    public get category(){
        return this._category
    }

    public get image(){
        return this._image
    }

    public get rating(){
        return this._rating
    } 

    public get vat(){
        return this._vat
    } 

    public getProduct(){
        const product = {
            id: this._id,
            title: this._title,
            price: this._price,
            description: this._description,
            category: this._category,
            image: this._image,
            rating: this._rating
        }

        return product
    }

    public getPriceWithTaxes(){
        const vatPercent: number = this.price * this._vat / 100
        return (this.price + vatPercent).toFixed(2)
    }
    
}