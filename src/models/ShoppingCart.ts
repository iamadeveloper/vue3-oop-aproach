import type { IUser } from "@/contracts/IUser"
import type { IShoppingCart } from "@/contracts/IShoppingCart"
import CartItem from "./CartItem"
import Order from "./Order"

export default class ShoppingCart implements IShoppingCart{
    private _items: CartItem[] = []
    private _user: IUser

    constructor(user: IUser){
        this._user = user
    }

    public get items(){
        return this._items
    }

    public get user(){
        return this._user
    }

    public add(product: CartItem){
        // const itemIndex = this._items.findIndex(item => item.item.id === product.item.id)
        // if(itemIndex !== -1){
        //     this._items[itemIndex].quantity = product.quantity + 1
        // }
        // else{
        //     this._items.push(product)
        // }
        this._items.push(product)
    }

    public remove(id: number){
        return this._items = this._items.filter(item => item.item.id !== id)
    }

    public getTotal(){
        return this._items.reduce((acc, item: CartItem) => acc + (item.item.getPriceWithTaxes() * item.quantity), 0)
    }

    public checkOut(): Order{
        return this.createOrder()
    }

    public createOrder(): Order{
        return new Order(this._items, this.getTotal(), new Date(), this._user)
    }

    public clearShoppingCart(){
        this._items = []
    }
}