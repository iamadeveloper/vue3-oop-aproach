import PaymentMethod from "./PaymentMethod";

export default class Paypal extends PaymentMethod{

    constructor(){
        super()
    }
    
    doPayment(amount: number) {
        console.log(`pago de ${amount}€, realizado con paypal`)
    }
    
}