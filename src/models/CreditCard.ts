import PaymentMethod from "./PaymentMethod";

export default class CreditCard extends PaymentMethod{

    constructor(){
        super()
    }
    
    doPayment(amount: number) {
        console.log(`pago de ${amount}€, realizado con tarjeta`)
    }
    
}