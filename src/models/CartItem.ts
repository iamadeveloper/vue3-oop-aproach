import type { IProduct } from "@/contracts/IProduct";

export default class CartItem{
    private _item: IProduct
    private _quantity: number

    constructor(item: IProduct, quantity: number){
        this._item = item
        this._quantity = quantity
    }

    public get item(){
        return this._item
    }

    public get quantity(){
        return this._quantity
    }

    public set quantity(quantity: number){
        this._quantity = quantity
    }

}