import type CartItem from "./CartItem"
import PaymentMethod from "./PaymentMethod";
import Paypal from "./Paypal";
import CreditCard from "./CreditCard";
import Invoice from "./Invoice";
import type {IUser} from "./contracts/IUser";
import type { PaymentType } from "@/enums/PaymentType";

export default class Order{
    private _id: number
    private _user: IUser
    private _items: CartItem[]
    private _total: number
    private _date: Date
    private _payment: PaymentMethod

    constructor(items: CartItem[], total: number, date: Date, user: IUser){
        this._id = new Date().getMilliseconds()
        this._items = items
        this._total = total
        this._date = date,
        this._user = user
    }

    public getOrderDetails(){
        const order = {
            items: this._items,
            total: this._total
        }

        return order
    }

    public get items(){
        return this._items
    }

    public get total(){
        return this._total
    }

    public get date(){
        return this._date
    }

    public get payment(){
        return this._payment
    }

    public executePayment(payment: PaymentType){
        if(payment === 'paypal'){
            this._payment = new Paypal().doPayment(this._total)
        }
        else{
            this._payment = new CreditCard().doPayment(this._total)
        }
    }

    public generateInvoice(){
        return new Invoice(this._date, this._user, this._items, this._id, this._total)
    }
}