export default abstract class PaymentMethod{

    abstract doPayment(amount: number)
}