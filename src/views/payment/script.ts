import { useCartStore } from '@/stores/cart'
import { useRouter } from 'vue-router'
import { ref, onMounted, reactive } from 'vue'
export default {
    name: 'payment',
    setup(){
        const router = useRouter()
        const { invoice, clearCart } = useCartStore()
        const invoiceDetails = reactive({})

        const resetShoppingCart = () => {
            clearCart()
            router.push({ name: 'home' })
        }

        const configView = () => {
            invoiceDetails.values = invoice.getInvoiceDetails()
        }
        
        onMounted(() => {
            configView()
        })
        return {
            invoice,
            invoiceDetails,
            resetShoppingCart
        }
    }
}