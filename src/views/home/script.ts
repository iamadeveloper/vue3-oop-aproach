import ProductCard from '../../components/app-product-card/index.vue'
import AppCategories from '../../components/app-categories/index.vue'
import { useProducts } from '@/composables/services/useProducts';
import { onMounted, reactive } from 'vue';
import { useProductsStore } from '@/stores/products'
// import { useUserStore } from '@/stores/user'
// import { useCartStore } from '@/stores/cart'
import { useRouter } from 'vue-router'
import Product from '@/models/Product';
import type {IProduct} from '@/contracts/IProduct'

export default {
    name: 'home',
    components: {
        ProductCard,
        AppCategories
    },
    setup(){
        const router = useRouter();
        const productsStore = useProductsStore()
        // const {user} = useUserStore()
        // const {initCart} = useCartStore()
        const { getProducts } = useProducts()
        const products = reactive([])

        const goToDetail = (id: number) => {
            router.push({ name: 'detail', params: { id: id } })
        }

        const fetchProducts = async () => {
            try {
                const productList = await getProducts()
                products.values = productList.map((product: IProduct) => new Product(product))
                productsStore.setProducts(products.values)
            } catch (error) {
                debugger
            }
        }

        const configView = () => {
            // initCart(user)
            if(productsStore.products.length){
                products.values = productsStore.products
            }
            else{
                fetchProducts()
            }
        }

        onMounted(() => configView())
        
        return {
            products,
            goToDetail
        }
    }
}