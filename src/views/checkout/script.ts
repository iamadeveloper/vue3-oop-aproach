import { useCartStore } from '@/stores/cart'
import { useRouter } from 'vue-router'
import { ref } from 'vue'
import { PaymentType } from '@/enums/PaymentType'
export default {
    name: 'checkout',
    setup(){
        const router = useRouter()
        const { order, executePayment } = useCartStore()
        const paymentMethod = ref<PaymentType>(PaymentType.Credit)

        const doPayment = () => {
            executePayment(paymentMethod.value)
            router.push({ name: 'payment' })
        }
        
        return {
            order,
            paymentMethod,
            doPayment
        }
    }
}