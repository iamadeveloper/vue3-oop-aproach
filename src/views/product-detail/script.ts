import { reactive, onMounted} from 'vue'
import { useProducts } from '@/composables/services/useProducts'
import { useRoute } from 'vue-router'
import { useProductsStore } from '@/stores/products'
import { useCartStore } from '@/stores/cart'
import ProductDetail from '../../components/app-product-detail/index.vue'
import Product from '@/models/Product'
import type {IProduct} from '@/contracts/IProduct'

export default {
    components: {ProductDetail},
    setup(){
        const route = useRoute()
        const productsStore = useProductsStore()
        const { getProductById } = useProducts()
        const product = reactive({})
        const { cart, addToStoredCart } = useCartStore()

        const fetchProduct = async () => {
            try {
                const singleProduct: IProduct = await getProductById(route.params.id)
                product.values = new Product(singleProduct)
                // debugger
            } catch (error) {
                console.log(error)
            }
        }

        const configView = () => {
            const findProduct = productsStore.getProductById(route.params.id)
            if(findProduct && Object.keys(findProduct).length){
                product.values = findProduct
            }
            else{
                fetchProduct()
            }
            
        }

        const addToCart = () => {
            const payload = {
                product: product.values,
                quantity: 1
            }
            addToStoredCart(payload)
            alert(`Product added to cart`)
            console.log(cart.items)
        }

        onMounted(() => {
            
            configView()
        })

        return {
            product,
            addToCart
        }
    }
}