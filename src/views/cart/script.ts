import ProductCard from '@/components/app-product-card/index.vue'
import { useCartStore } from '@/stores/cart'
import { useRouter } from 'vue-router'
import {ref} from 'vue'
export default {
    name: 'cart',
    components: {ProductCard},
    setup(){
        const { cart, removeItem, checkout } = useCartStore()
        const router = useRouter()
        const quantity = ref(1)
        
        const checkoutOrder = () => {
            checkout()
            router.push({ name: 'checkout'})
        }
        const goToDetail = (id: number) => {
            router.push({ name: 'detail', params: { id: id } })
        }

        return {
            removeItem,
            checkoutOrder,
            goToDetail,
            cart,
            quantity
        }
    }
}