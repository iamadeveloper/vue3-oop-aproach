import { useUserStore } from '@/stores/user'
import { useCartStore } from '@/stores/cart'
import { useRouter } from 'vue-router'
import { ref, computed } from 'vue'
import User from '@/models/User'
export default {
    name: 'login',
    setup(){
        const router = useRouter()
        const { login } = useUserStore()
        const {initCart} = useCartStore()
        const userName = ref('')
        const userEmail = ref('')
        
        const isFormValid = computed(() => {
            return userName.value.length > 0 && userEmail.value.length > 0
        })

        const doLogin = async () => {
            const user = new User(userName.value, userEmail.value)
            await login(user)
            initCart(user)
            router.push({ name: 'home' })
        }
        
        return {
            userName,
            isFormValid,
            userEmail,
            doLogin
        }
    }
}