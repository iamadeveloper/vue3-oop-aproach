import type CartItem from "@/models/CartItem";
import type { IUser } from "./IUser";

interface IShoppingCart{
    items: CartItem[]
    user: IUser
}

export type {IShoppingCart}