interface IUser{
    name: string
    email: string
}

export type { IUser }