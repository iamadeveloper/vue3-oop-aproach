import HandleCall from '../../api/handleCall.js'
import type {IProduct} from '../../contracts/IProduct'
import type { ICategory } from '@/contracts/ICategory.js'



export function useProducts(){
    const basePath = import.meta.env.VITE_API_URL

    const getProducts = () => {
        return new Promise((resolve, reject) => {
            
            HandleCall.get(`${basePath}/products`)
            .then((response: Promise<IProduct[]>) => resolve(response))
            .catch((e: Promise<Error>) => reject(e))

        })
    }

    const getProductById = (id: number) => {
        return new Promise((resolve, reject) => {
            
            HandleCall.get(`${basePath}/products/${id}`)
            .then((response: IProduct) => resolve(response))
            .catch((e: Promise<Error>) => reject(e))

        })
    }

    const getProductByCat = (cat: number) => {
        return new Promise((resolve, reject) => {
            
            HandleCall.get(`${basePath}/products/category/${cat}`)
            .then((response: IProduct) => resolve(response))
            .catch((e: Promise<Error>) => reject(e))

        })
    }

    const getCategories = () => {
        return new Promise((resolve, reject) => {
            
            HandleCall.get(`${basePath}/products/categories`)
            .then((response: Promise<ICategory[]>) => resolve(response))
            .catch((e: Promise<Error>) => reject(e))

        })
    }

    return{
        getProducts,
        getProductById,
        getProductByCat,
        getCategories
    }
}