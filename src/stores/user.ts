import { reactive } from 'vue'
import { defineStore } from 'pinia'
import type { IUser } from '@/contracts/IUser'

export const useUserStore = defineStore('user', {

  state: () => {
    return { 
      user: reactive({}) as IUser,
      isUserLogged: false
     }
  },
  actions: {
    login(payload: IUser) {
      this.user = payload
      this.isUserLogged = true
    },
    logout(){
      this.isUserLogged = false
    }
    
  }
})
