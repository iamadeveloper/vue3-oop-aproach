import { reactive } from 'vue'
import { defineStore } from 'pinia'
import type { ICategory } from '@/contracts/ICategory'

export const useCategoriesStore = defineStore('categories', {

  state: () => {
    return { 
      categories: reactive<ICategory[]>([])
     }
  },
  actions: {
    setCategories(payload: ICategory[]) {
      this.categories = payload
    }
    
  }
})
