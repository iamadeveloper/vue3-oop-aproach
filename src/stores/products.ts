import { reactive } from 'vue'
import { defineStore } from 'pinia'
import type { IProduct } from '@/contracts/IProduct'

export const useProductsStore = defineStore('products', {

  state: () => {
    return { 
      products: reactive<IProduct[]>([])
     }
  },
  getters: {
    getProductById: (state) => {
      return (id: number) => state.products.find(product => product.id === Number(id))
    }
  },
  actions: {
    setProducts(payload: IProduct[]) {
      this.products = payload
    }
    
  }
})
