import { reactive, ref } from 'vue'
import { defineStore } from 'pinia'
import ShoppingCart from '@/models/ShoppingCart'
import CartItem from '@/models/CartItem'
import type { IUser } from '@/contracts/IUser'
import type Order from '@/models/Order'
import type Invoice from '@/models/Invoice'
import type { IProduct } from '@/contracts/IProduct'
import type { PaymentType } from '@/enums/PaymentType'

export const useCartStore = defineStore('cart', {

  state: () => {
    return { 
      cart: reactive({}) as ShoppingCart,
      cartCount: 0,
      order: reactive({}) as Order,
      invoice: reactive({}) as Invoice
     }
  },
  getters: {},
  actions: {
    initCart(user: IUser){
        this.cart = new ShoppingCart(user)
    },
    addToStoredCart(payload: IProduct){
        this.cart.add(new CartItem(payload.product, payload.quantity))
        this.cartCount = this.cart.items.length
    },
    removeItem(item: IProduct){
      this.cart.remove(item.id)
      this.cartCount = this.cart.items.length
    },
    checkout(){
      this.cart.checkOut()
      this.order = this.cart.createOrder()
    },
    executePayment(PaymentType: PaymentType){
      this.order.executePayment(PaymentType)
      this.invoice = this.order.generateInvoice()
    },
    clearCart(){
      this.cart.clearShoppingCart()
      this.cartCount = 0
    }
    
  }
})
